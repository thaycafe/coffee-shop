![Logo do Coffee Shop](https://raw.githubusercontent.com/thaycafe/coffee-shop/master/frontend/src/assets/public/images/CoffeeShop_Logo.png)

# OWASP Coffee Shop

**Coffee Shop** é uma aplicação derivada do OWASP Juice Shop, provavelmente a aplicação web insegura mais moderna e sofisticada! Pode ser usada em treinamentos de segurança, demonstrações de conscientização, eventos Capture The Flag (CTF) e como cobaia para ferramentas de segurança. O Coffee Shop abrange vulnerabilidades de toda a [OWASP Top Ten](https://owasp.org/www-project-top-ten/) junto com outras falhas de segurança e características encontradas em aplicações do mundo real.

## Licenciamento

[![licença](https://img.shields.io/github/license/bkimminich/juice-shop.svg)](https://github.com/juice-shop/juice-shop/blob/master/LICENSE)

Este projeto está licenciado sob os termos da Licença MIT (MIT).

- Direitos autorais © 2014-2023 Bjoern Kimminich & os contribuidores do OWASP Juice Shop
- Direitos autorais © 2023-2024 Thaynara Mendes e Samuel Gonçalves

## Começando

Para começar com o OWASP Coffee Shop, você pode baixar a última versão em nosso [repositório no GitHub](https://github.com/thaycafe/coffee-shop/releases). Instruções de instalação e documentação mais detalhada estão disponíveis em nosso [wiki](https://github.com/thaycafe/coffee-shop/wiki).

## Contribuindo

Nós recebemos contribuições da comunidade! Seja relatos de bugs, solicitações de recursos ou contribuições de código, sinta-se à vontade para abrir uma issue ou criar um pull request em nosso [repositório no GitHub](https://github.com/thaycafe/coffee-shop). Por favor, consulte nossas [diretrizes de contribuição](https://github.com/thaycafe/coffee-shop/CONTRIBUTING.md) para mais informações.

## Suporte

Se você precisar de ajuda ou tiver alguma dúvida, junte-se ao nosso [chat da comunidade](https://owasp.slack.com/). Você também pode encontrar informações úteis e discussões no [Fórum de Discussão do OWASP Coffee Shop](https://github.com/thaycafe/coffee-shop/discussions).

## Agradecimentos

- Um agradecimento especial a Bjoern Kimminich e aos contribuidores originais do OWASP Juice Shop por seu trabalho fantástico e inspiração.
- Obrigado a todos os nossos contribuidores e membros da comunidade que tornam o OWASP Coffee Shop um recurso valioso para aprender sobre segurança de aplicações web.

---

*OWASP Coffee Shop é parte dos [projetos OWASP](https://owasp.org/) e adere aos seus princípios e diretrizes.*


